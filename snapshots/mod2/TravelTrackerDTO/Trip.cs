﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelTrackerDTO
{
	public class Trip
	{

		public int ID { get; set; }

		[Required]
		[StringLength(200)]
		public string Name { get; set; }

		[Required]
		public DateTime StartDate { get; set; }

		[Required]
		public DateTime EndDate { get; set; }

	}

}
