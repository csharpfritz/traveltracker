﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelTrackerDTO
{
	public class Segment
	{

		public int ID { get; set; }

		[Required]
		public int TripID { get; set; }

		[Required]
		[StringLength(200)]
		public string Name { get; set; }

		[Required]
		public DateTime StartDateTime { get; set; }

		[Required]
		public DateTime EndDateTime { get; set; }

		[Required]
		public string Type { get; set; }

		[StringLength(4000)]
		public string Address { get; set; }

	}


}
