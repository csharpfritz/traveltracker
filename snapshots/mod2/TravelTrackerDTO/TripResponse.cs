﻿using System.Collections.Generic;

namespace TravelTrackerDTO
{
	public class TripResponse : Trip
	{
		public ICollection<Segment> Segments { get; set; }
	}

}
