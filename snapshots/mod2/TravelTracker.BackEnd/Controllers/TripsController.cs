﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TravelTracker.BackEnd.Infrastructure;
using TravelTracker.BackEnd.Models;
using TravelTrackerDTO;

namespace TravelTracker.BackEnd.Controllers
{

	[Route("api/[controller]")]
	public class TripsController : Controller
	{
		public ApplicationDbContext Repository { get; }

		public TripsController(ApplicationDbContext repository)
		{
			this.Repository = repository;
		}
		
		// GET api/values
		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var trips = await Repository.Trips.AsNoTracking()
					.Include(t => t.Segments)
					.ToListAsync();

			var result = trips.Select(t => t.MapTripResponse());
			return Ok(result);

		}

		// GET api/values/5
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var trip = await Repository.Trips.AsNoTracking()
				.Include(t => t.Segments)
				.SingleOrDefaultAsync(t => t.ID == id);

			if (trip == null)
			{
				return NotFound();
			}

			var result = trip.MapTripResponse();
			return Ok(result);

		}

		// POST api/values
		[HttpPost]
		public async Task<IActionResult> Post([FromBody]TravelTrackerDTO.Trip value)
		{
			
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var trip = new Models.Trip
			{
				Name = value.Name,
				StartDate = value.StartDate,
				EndDate = value.EndDate
			};

			Repository.Trips.Add(trip);
			await Repository.SaveChangesAsync();

			var result = trip.MapTripResponse();

			return CreatedAtAction(nameof(Get), new { id = trip.ID }, result);


		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public async Task<IActionResult> Put(int id, [FromBody]TravelTrackerDTO.Trip value)
		{

			var trip = await Repository.FindAsync<Models.Trip>(id);

			if (trip == null)
			{
				return NotFound();
			}

			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			trip.Name = value.Name;
			trip.StartDate = value.StartDate;
			trip.EndDate = value.EndDate;
			await Repository.SaveChangesAsync();

			var result = trip.MapTripResponse();

			return Ok(result);

		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			var trip = await Repository.FindAsync<Models.Trip>(id);

			if (trip == null)
			{
				return NotFound();
			}

			Repository.Remove(trip);
			await Repository.SaveChangesAsync();

			return NoContent();
			

		}
	}
}
