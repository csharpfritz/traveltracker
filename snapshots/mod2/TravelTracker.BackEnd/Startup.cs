﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using TravelTracker.BackEnd.Models;

namespace TravelTracker.BackEnd
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{

			// services.AddTransient<TripsRepository>();

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseSqlite("Data Source=trips.db");
			});

			services.AddMvcCore()
				.AddDataAnnotations()
				.AddJsonFormatters()
				.AddApiExplorer();

			services.AddSwaggerGen(options =>
				options.SwaggerDoc("v1", new Info { Title = "Travel Tracker API", Version = "v1" })
			);

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSwagger();
			app.UseSwaggerUI(options =>
				options.SwaggerEndpoint("/swagger/v1/swagger.json", "Travel Tracker API v1")
			);

			app.UseMvc();
		}
	}
}
