﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelTracker.BackEnd.Models;

namespace TravelTracker.BackEnd.Infrastructure
{
	public static class EntityExtensions
	{

		public static TravelTrackerDTO.TripResponse MapTripResponse(this Trip trip) =>
			new TravelTrackerDTO.TripResponse
			{
				ID = trip.ID,
				Name = trip.Name,
				StartDate = trip.StartDate,
				EndDate = trip.EndDate,
				Segments = trip.Segments?.Select(s =>
					new TravelTrackerDTO.Segment
					{
						ID = s.ID,
						Name = s.Name,
						StartDateTime = s.StartDateTime,
						EndDateTime = s.EndDateTime
					}).ToList()
			};

	}
}
