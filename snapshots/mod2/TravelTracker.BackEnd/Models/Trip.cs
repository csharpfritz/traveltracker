﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelTracker.BackEnd.Models
{
	public class Trip : TravelTrackerDTO.Trip
	{

		public virtual ICollection<Segment> Segments { get; set; }

	}

}
