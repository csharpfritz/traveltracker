﻿using System.ComponentModel.DataAnnotations;

namespace TravelTracker.BackEnd.Models
{
	public class Segment : TravelTrackerDTO.Segment {

		[Required]
		public Trip Trip { get; set; }

	}

}
