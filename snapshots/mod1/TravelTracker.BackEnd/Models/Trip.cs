﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace TravelTracker.BackEnd.Models
{
	public class Trip
	{

		public int ID { get; set; }

		public string Name { get; set; }

		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }


	}

}
