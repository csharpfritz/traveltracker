﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TravelTracker.BackEnd.Models
{
	public class TripsRepository
	{

		private List<Trip> MyTrips = new List<Trip>
		{
			new Trip
			{
				ID=1,
				Name="MVP Summit 2018",
				StartDate = new DateTime(2018,3,4),
				EndDate = new DateTime(2018,3,8)
			},
			new Trip
			{
				ID=2,
				Name="DevIntersection Orlando 2018",
				StartDate = new DateTime(2018,3,24),
				EndDate = new DateTime(2018,3,28)
			},
			new Trip
			{
				ID=3,
				Name="Microsoft Build 2018",
				StartDate = new DateTime(2018,5,6),
				EndDate = new DateTime(2018,5,10)
			}
		};

		public Trip GetById(int id)
		{
			return MyTrips.FirstOrDefault(t => t.ID == id);
		}

		public IEnumerable<Trip> Get()
		{
			return MyTrips;
		}

		public void Add(Trip newTrip)
		{
			newTrip.ID = MyTrips.Max(t => t.ID) + 1;
			MyTrips.Add(newTrip);
		}

		public void Update(int id, Trip trip)
		{
			var existingTrip = MyTrips.FirstOrDefault(t => t.ID == id);
			if (existingTrip == null) return;

			existingTrip.Name = trip.Name;
			existingTrip.StartDate = trip.StartDate;
			existingTrip.EndDate = trip.EndDate;

		}

		public void Delete(int id)
		{
			MyTrips.Remove(GetById(id));
		}



	}

}
