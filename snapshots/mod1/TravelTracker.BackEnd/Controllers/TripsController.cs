﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TravelTracker.BackEnd.Models;

namespace TravelTracker.BackEnd.Controllers
{

	[Route("api/[controller]")]
	public class TripsController : Controller
	{
		public TripsRepository Repository { get; }

		public TripsController(TripsRepository repository)
		{
			this.Repository = repository;
		}
		
		// GET api/values
		[HttpGet]
		public IEnumerable<Trip> Get()
		{
			return Repository.Get();
		}

		// GET api/values/5
		[HttpGet("{id}")]
		public Trip Get(int id)
		{
			return Repository.GetById(id);
		}

		// POST api/values
		[HttpPost]
		public void Post([FromBody]Trip value)
		{
			Repository.Add(value);
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody]Trip value)
		{
			Repository.Update(id, value);
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
			Repository.Delete(id);
		}
	}
}
